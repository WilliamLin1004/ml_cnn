from __future__ import print_function
import keras
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras import backend as K

batch_size = 1
epochs = 20

img_rows, img_cols = 28, 28
input_shape = (img_rows, img_cols, 1)



# Initialising the CNN
model = Sequential()
model.add(Conv2D(32, kernel_size=(2, 2),
                 activation='relu',
                 input_shape=input_shape))
model.add(Conv2D(64, (2, 2), activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Conv2D(64, (2, 2), activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))
model.add(Dropout(0.5))
model.add(Flatten())
model.add(Dense(128, activation='relu'))
model.add(Dense(128, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(1, activation='sigmoid'))
model.compile(loss='binary_crossentropy',
              optimizer='rmsprop',
              metrics=['accuracy'])





#according to various sources, fitting cnn to the test sets
from keras.preprocessing.image import ImageDataGenerator
train_datagen = ImageDataGenerator(rescale = 1./255,
shear_range = 0.2,
zoom_range = 0.2,
horizontal_flip = True)
test_datagen = ImageDataGenerator(rescale = 1./255)
training_set = train_datagen.flow_from_directory('dataset/training_sets',
target_size = (28, 28),
batch_size = 1,
class_mode = 'binary')
test_set = test_datagen.flow_from_directory('dataset/test_sets',
target_size = (28, 28),
batch_size = 1,
class_mode = 'binary')
print(type(test_set))
model.fit_generator(training_set,
steps_per_epoch = 100,
epochs = 25,
validation_data = test_set,
validation_steps = 35)
model.save("mnist_cnn.h5")
score = model.evaluate_generator(training_set,verbose = 0)
print('Test loss:', score[0])
print('Test accuracy:', score[1])




im_address="image.bmp"				#圖片路徑
im = Image.open(im_address) 		
#資料前處理
im2arr = np.array(im)
im2arr=im2arr/255
im2arr=im2arr.reshape(1,28,28,1) 
ans=model.predict(im2arr)[0]
training_set.class_indices
if result[0][0] == 1:
	prediction = 'smile'
else:
	prediction = 'frown'